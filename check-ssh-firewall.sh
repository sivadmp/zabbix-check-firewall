#!/bin/bash

#Cambiar por el nombre del servidor registrado en Zabbix
SrvHost=$( cat /etc/zabbix/zabbix_agent2.conf | grep -v '^#' | awk -F\= '/Hostname/ { print $2 }' )

mkdir -p /tmp/zabbix

Sender="/usr/bin/zabbix_sender"
Senderarg1='-vv'
Senderarg2='-c'
Senderarg3="/etc/zabbix/zabbix_agent2.conf"
Senderarg4='-i'
Senderarg5='-k'
Senderarg6='-o'
Senderarg7='0'
Senderarg8='1'

Senderargfirewall='/tmp/zabbix/LinuxFirewall.txt'
SenderargSshRoot='/tmp/zabbix/LinuxSshRoot.txt'
SenderargSshPassword='/tmp/zabbix/LinuxSshPassword.txt'
SenderargSshPubKey='/tmp/zabbix/LinuxSshPubKey.txt'

function detectOS {
    if [[ -e /etc/centos-release ]]; then
        export os="centos"
        vos=$( cat /etc/os-release | grep -w PRETTY_NAME | awk -F\= '{ print $2 }' |  sed 's/"//g' | sed 's/Linux//g' )
    fi
    if [[ -e /etc/debian_version ]]; then
        export os="debian"
        vos=$( cat /etc/os-release | grep -w PRETTY_NAME | awk -F\= '{ print $2 }' |  sed 's/"//g' | sed 's/GNU\/Linux//g' )
    fi
}

function check_firewall {
    if [[ "$os" == "debian" ]]; then
        FILE="/etc/ufw/ufw.conf"
        if [ -f "$FILE" ]; then
            FIREWALL=$( awk -F\= '/ENABLED/ {print $2}' $FILE )
        else
            FIREWALL="no"
        fi
    fi
    if [[ "$os" == "centos" ]]; then
        FIREWALL=$( systemctl status firewalld | grep -w 'Active:' | awk '{print $2}' )
    fi
    if [[ "$FIREWALL" == "inactive" || "$FIREWALL" == "no" ]]; then
        FIREWALL=1
    else
        FIREWALL=0
    fi
    echo "- Linux.firewall "$FIREWALL > $Senderargfirewall
}

function check_ssh_root(){
    SSH_ROOT=$( cat /etc/ssh/sshd_config | grep ^PermitRootLogin | awk '{ print $2}' )
    SW=0
    if [[ "$SSH_ROOT" == "yes" ]]; then
        SW=1
    fi
    echo "- Linux.sshroot "$SW > $SenderargSshRoot
}

function check_ssh_pass(){
    PASS_AUTH=$( cat /etc/ssh/sshd_config | grep ^PasswordAuthentication | awk '{ print $2}' )
    SW=0
    if [[ "$PASS_AUTH" == "yes" ]]; then
        SW=1
    fi
    if [ -z "$PASS_AUTH" ];then
        SW=1
    fi
    echo "- Linux.sshpassword "$SW > $SenderargSshPassword
}

function check_ssh_pubkey(){
    PUBKEY=$( cat /etc/ssh/sshd_config | grep ^PubkeyAuthentication | awk '{ print $2}' )
    SW=0
    if [[ "$PUBKEY" == "no" ]]; then
        SW=1
    fi
    if [ -z "$PUBKEY" ];then
        SW=1
    fi
    echo "- Linux.sshpubkey "$SW > $SenderargSshPubKey
}

function sender_zabbix {
    $Sender $Senderarg1 $Senderarg2 $Senderarg3 $Senderarg4 $Senderargfirewall -s "$SrvHost"
    $Sender $Senderarg1 $Senderarg2 $Senderarg3 $Senderarg4 $SenderargSshRoot -s "$SrvHost"
    $Sender $Senderarg1 $Senderarg2 $Senderarg3 $Senderarg4 $SenderargSshPassword -s "$SrvHost"
    $Sender $Senderarg1 $Senderarg2 $Senderarg3 $Senderarg4 $SenderargSshPubKey -s "$SrvHost"
}

function main(){
    detectOS
    check_firewall
    check_ssh_root
    check_ssh_pass
    check_ssh_pubkey
    sender_zabbix
}

main
