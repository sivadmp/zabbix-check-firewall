# Zabbix check ssh firewall

> Autor: Davis Mendoza Paco **<davis.men.pa@gmail.com> / <davis.mp@yandex.com>**

Este es una plantilla de Zabbix para monitorear el firewall de Sistemas operativos Gnu/Linux, como tambien la autenticación por SSH:

* Autenticación root: PermitRootLogin
* Autenticación por contraseña: PasswordAuthentication
* Autenticación por clave publica: PubkeyAuthentication

Ha sido probado en Zabbix v5.2.7 con Centos 7 y Debian 10 y 11. 

## Requisitos

* **Debian** Tener instalado UFW y zabbix-sender
```sh
sudo apt install ufw
sudo apt install zabbix-sender
```
* **CentOS 7** Tener instalado firewalld y zabbix-sender

```sh
wget https://repo.zabbix.com/zabbix/3.0/rhel/7/x86_64/zabbix-sender-3.0.32-1.el7.x86_64.rpm
rpm -i zabbix-sender-3.0.32-1.el7.x86_64.rpm
```

## Archivos incluidos

- **LinuxSshFirewall.xml:** Plantilla para importar a zabbix
- **check-ssh-firewall.sh:** script schell, Debe colocarse en el directorio de "plugins" en la carpeta de instalación de Zabbix Agent 

## Instalación

Crear el directorio de plugins
```sh
sudo mkdir /etc/zabbix/plugins
```

Descargar el script y copiar a la carpeta creada
```sh
mv check-firewall.sh /etc/zabbix/plugins
```

Asignarle permisos de ejecución al script creado
```sh
chmod 755 /etc/zabbix/plugins/check-ssh-firewall.sh
```

Cambiando el propietario de la carpeta y de su contenido
```shell
sudo chown -R zabbix.zabbix /etc/zabbix/plugins
```

**CentOS**

En CentOS asignarle permisos de lectura al archivo de configuración de SSH
```shell
sudo chmod go+r /etc/ssh/sshd_config
```
 
## Configuración

Habilitar la ejecución de comandos remotos 

> https://www.zabbix.com/documentation/5.2/en/manual/config/items/restrict_checks
 
Editar el archivo de configuración
```sh
sudo nano /etc/zabbix/zabbix_agent2.conf
```

Adicionar la siguiente linea en la configuración
```
AllowKey=system.run[*]
```

Para que los cambios surtan efecto, reiniciar el servicio
```sh
sudo systemctl restart zabbix-agent2.service
```

## Probar 

Para verificar si el script funciona correctamente, realizar la prueba ejecutando
```sh
sudo -u zabbix -H bash /etc/zabbix/plugins/check-ssh-firewall.sh
```

El resultado del comando nos muestra lo siguiente:
```sh
zabbix_sender [14058]: DEBUG: answer [{"response":"success","info":"processed: 1; failed: 0; total: 1; seconds spent: 0.000057"}]
info from server: "processed: 1; failed: 0; total: 1; seconds spent: 0.000057"
sent: 1; skipped: 0; total: 1
```

Del resultado obtenido, se puede identificar el resultado correcto con **0**  fallas
```sh
"response":"success","info":"processed: 1; failed: 0
```
